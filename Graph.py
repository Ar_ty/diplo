import matplotlib.pyplot as plt
import os

def score(moving_avg, name=" "):
    x = []
    y = []
    name = name + "_Rewards"
    plt.title(name)
    [(x.append(i), y.append(j)) for i, j in enumerate(moving_avg)]
    plt.scatter(x, y, marker='|', c=[-i for i in y])
    plt.ylabel("Score")
    plt.xlabel("Number of Games Played")
    # plt.savefig(name + " Score.png")
    plt.show()
    plt.close()


def average(reward, moving_avg, totalSteps, name=" ", n=10):
    x = []
    y = []
    sum_ = 0
    for i, j in enumerate(moving_avg):
        sum_ += j
        if i % n == 0:
            x.append(i)
            y.append(sum_ / n)
            sum_ = 0
    if reward:
        name = name + "_Rewards"
        plt.title(name)
        plt.plot(x, y, '-')
        plt.figtext(.4, .25, "No. of Episodes = " + str(len(moving_avg)))
        plt.figtext(.4, .2, "Average Score = {:0.2f}".format(sum(moving_avg) / len(moving_avg)))
        plt.figtext(.4, .15, "Total steps = " + str(totalSteps))
        plt.ylabel("Average over {:d} Episodes".format(n))
        plt.xlabel("Number of Games Played")
        # plt.savefig(name + "_AvgOver " + str(n) + ".png")
    else:
        name = name + "_Losses"
        plt.title(name)
        plt.plot(x, y, '-')
        plt.figtext(.4, .25, "No. of Episodes = " + str(len(moving_avg)))
        plt.figtext(.4, .2, "Average Loss = {:0.2f}".format(sum(moving_avg) / len(moving_avg)))
        plt.figtext(.4, .15, "Total steps = " + str(totalSteps))
        plt.ylabel("Average over {:d} Episodes".format(n))
        plt.xlabel("Number of Games Played")
        # plt.savefig(name + "_AvgOver " + str(n) + ".png")
    plt.show()
    plt.close()


def averageComparison(algos, reward, n=10):
    allRewards = []
    allLosses = []
    names = []
    for algo in algos:
        names.append(algo[3])
        for data in range(2):
            x = []
            y = []
            sum_ = 0
            for i, j in enumerate(algo[data]):
                sum_ += j
                if i % n == 0:
                    x.append(i)
                    y.append(sum_ / n)
                    sum_ = 0
            if data == 0:
                allRewards.append(x)
                allRewards.append(y)
            else:
                allLosses.append(x)
                allLosses.append(y)

    if reward:
        plt.title('Reward comparison between Deep Sarsa, \nDQN, DDQN, dueling DQN, double dueling DQN')
        for i in range(len(allRewards)):
            if i % 2 == 0:
                plt.plot(allRewards[i], allRewards[i + 1])
        plt.legend(names, loc="lower right")
        plt.ylabel("Average over {:d} Episodes".format(n))
        plt.xlabel("Number of Games Played")
        # plt.savefig("Reward Comparison Average Over " + str(n) + ".png")
    else:
        plt.title('Losses comparison between Deep Sarsa, \nDQN, DDQN, dueling DQN, double dueling DQN')
        for i in range(len(allLosses)):
            if i % 2 == 0:
                plt.plot(allLosses[i], allLosses[i + 1])
        plt.legend(names, loc="lower right")
        plt.ylabel("Average over {:d} Episodes".format(n))
        plt.xlabel("Number of Games Played")
        # plt.savefig("Losses Comparison Average Over " + str(n) + ".png")
    plt.show()
    plt.close()


# def moving_average(a, n=3):
#     ret = np.cumsum(a, dtype=float)
#     ret[n:] = ret[n:] - ret[:-n]
#     return ret[n - 1:] / n
#
#
# def plot_single(data, ylabel, title):
#     plt.figure()
#     plt.plot(data)
#     plt.xlabel('episode')
#     plt.ylabel(ylabel)
#     plt.title(title)
#     plt.show()
#     plt.close()


def comparison(Sarsa, DQN, DDQN, dueling, DoubleDueling, ylabel, title):
    plt.figure()
    plt.plot(range(len(Sarsa)), Sarsa, range(len(DQN)), DQN, range(len(DDQN)), DDQN, range(len(dueling)), dueling,
             range(len(DoubleDueling)), DoubleDueling)
    plt.xlabel('episode')
    plt.ylabel(ylabel)
    plt.title(title)
    plt.legend(['Deep Sarsa', 'DQN', 'DDQN', 'Dueling', 'Double Dueling'])
    plt.show()
    plt.close()


def main():
    allArray = []

    for root, dirs, files in os.walk("Logs/"):
        for filename in files:
            nazov = filename.split("_").pop()
            nazov = nazov.replace(".txt", "")
            if ".txt" in filename:
                avgAlgo = []
                avgAlgoLoss = []
                totalSteps = 0
                for line in open("Logs/" + filename, 'r').readlines(-1):
                    try:
                        avgAlgo.append(float((line.rsplit('\t', 6)[1])[14:]))
                        loss = (line.rsplit('\t', 6)[3])[6:]
                        if loss != "nan":
                            avgAlgoLoss.append(float(loss))
                        totalSteps = totalSteps + int((line.rsplit('\t', 6)[2])[7:])
                    except:
                        pass
                allArray.append([avgAlgo, avgAlgoLoss, totalSteps, nazov])
                print('\n', nazov,
                      '\nNo. of Episodes = ', avgAlgo.__len__(),
                      '\nAverage Score = {:0.2f}'.format(sum(avgAlgo) / avgAlgo.__len__()),
                      '\nTotal steps = ', totalSteps)

    while True:
        c = int(input('\n\n1. Plot Score vs Episode\n2. Plot Average over n Episodes\n'
                      '3. Comparison of All\n0. Exit\n\n'))

        if c == 1:
            for i in range(len(allArray)):
                score(allArray[i][0], allArray[i][3])
        elif c == 2:
            forEpisode = int(input('\nEnter number of Episodes to average: '))
            for i in range(len(allArray)):
                average(True, allArray[i][0], allArray[i][2], allArray[i][3], forEpisode)
                average(False, allArray[i][1], allArray[i][2], allArray[i][3], forEpisode)
        elif c == 0:
            return None
        elif c == 3:
            forEpisode = int(input('\nEnter number of Episodes to average: '))
            averageComparison(allArray, True, forEpisode)
            averageComparison(allArray, False, forEpisode)


if __name__ == '__main__':
    main()
