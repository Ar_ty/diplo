from keras.optimizers import RMSprop
from keras.optimizers import Adam
from keras.optimizers import SGD
from keras.callbacks import CSVLogger


TARGET_UPDATE_PERIOD = 10000

LEARNING_RATE = 0.000001

DISCOUNT_FACTOR = 0.99

BATCH_SIZE = 32

NOOPMAX = 10

MAX_EXPERIENCES = 100000  # Memory Size 40k

MIN_EPSILON = 0.01

DECAY_RATE = 0.99999

TOTAL_LIVES = 3

RMS_Opt = RMSprop(lr=LEARNING_RATE)
# RMS_Opt = RMSprop(lr=LEARNING_RATE, rho=0.95, epsilon=0.01)
ADAM_Opt = Adam(lr=LEARNING_RATE)
SGD_Opt = SGD(lr=LEARNING_RATE)

EXPLORATION_TEST = 0.1
