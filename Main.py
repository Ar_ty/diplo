import gym
from PIL import Image
import numpy as np
from Agent import Agent
import sys
import gc
from time import time, sleep
import os
from Paremeters import *
from tqdm import tqdm
import matplotlib.pyplot as plt


def main(game, dqn, ddqn, dueling, both):
    # print(dqn, ddqn, dueling, both)
    env_name = game + "-v0"
    print(env_name)

    algo = ""
    if dqn:
        algo = "_DQN"
    elif ddqn:
        algo = "_DDQN"
    elif dueling:
        algo = "_Dueling"
    elif both:
        ddqn = True
        dueling = True
        algo = "_DDQN+Dueling"

    if not os.path.exists("Experiences"):
        os.mkdir("Experiences")
    if not os.path.exists("Logs"):
        os.mkdir("Logs")
    if not os.path.exists("Weights"):
        os.mkdir("Weights")

    log = open("Logs/log_" + game + algo + ".txt", 'a')
    episodes = int(input("Episodes? : "))
    timer = time()

    env = gym.make(env_name)

    agent = Agent((84, 84, 4), env.action_space.n, env_name, load_weights=True, ddqn=ddqn, dueling=dueling)
    count = 0
    _ = 0
    av_score = 0
    min_experiences = 0 if agent.load_state(False, True) else 1500
    for episode in tqdm(range(episodes), leave=False, mininterval=0.001):
        obsv = process_state(env.reset())
        current_state = np.array([obsv, obsv, obsv, obsv])
        # print("obs")
        score = 0
        randActions = 0
        done = False
        steps = _
        loss = []
        epsilon = 0
        while not done:
            _ += 1
            # env.render() if episodes - episode - 1 <= 3 else None

            action, randPercentage = agent.action(np.asarray([current_state]), _)
            if randPercentage:
                randActions = randActions + 1
            # if _ % 10 == 0:
            #     print("Action Taken: ", action)

            obsv, reward, done, info = env.step(action)
            # image = Image.fromarray(obsv, 'RGB').convert('L').resize((84, 84))
            # image.save("image.jpg")
            obsv = process_state(obsv)
            next_state = get_next_state(current_state, obsv)

            clipped_reward = np.clip(reward, -1, 1)
            # print("Clipped Reward: ", clipped_reward)

            agent.experience_gain(np.asarray([current_state]), action, clipped_reward, np.asarray([next_state]), done)

            if agent.experience_available() and _ > min_experiences:
                epsilon = agent.greedyEpsilon()
                if _ % 4 == 0:  # trenuj každé 4 framy
                    count += 1
                    loss.append(agent.train())
                    # print("Steps : ", _, "\tCount = ", count)
                    if count == TARGET_UPDATE_PERIOD:
                        count = 0
                        agent.save()
                        agent.update_target_network()

            current_state = next_state
            score += reward
        steps = _ - steps

        agent.save()
        # env.close()
        timer = time() - timer
        av_score = (av_score + score) / 2 if episode != 0 else score
        # print("\n")
        print(episode + 1, "\tTotalReward = ", score, "\tSteps: ", steps, "\tLoss: {:.4f}".format(np.mean(loss)),
              "\tMoving Avg: {:.2f}".format(av_score), "\tRandom actions: {:.0f}".format(100 * randActions/steps), "%",
              "\tLast epsilon: {:.4f}".format(epsilon),
              "\tTime: %d" % (timer / 60), "\b:{:.0f}".format((timer % 60)))
        log.write(str(episode + 1) + "\tTotalReward = " + str(score) + "\tSteps: " + str(
            steps) + "\tLoss: {:.4f}".format(np.mean(loss)) + "\tMoving Avg: {:.2f}".format(av_score) +
                  "\tLast epsilon: {:.4f}".format(epsilon) +
                  "\tTime: %d" % int(timer / 60) + ":{:.0f} \n".format((timer % 60)))
        timer = time()
        del loss

        # print(episode)
        if episode % 500 == 0 and episode != 0 and episode != episodes - 1:
            print("Continuous save")
            agent.save_state(ongoing=True, sarsa=False)
        # print("\n")
    env.close()
    print("\n\nTotal Steps = ", _)
    log.close()

    del timer
    del current_state
    del next_state
    del count
    del _
    del steps
    del log
    del env
    gc.collect()

    agent.save_state(ongoing=False, sarsa=False)


def sarsa(game):
    env_name = game + "-v0"

    if not os.path.exists("Experiences"):
        os.mkdir("Experiences")
    if not os.path.exists("Logs"):
        os.mkdir("Logs")
    if not os.path.exists("Weights"):
        os.mkdir("Weights")

    log = open("Logs/log_" + game + "_Sarsa" + ".txt", 'a')
    episodes = int(input("Episodes? : "))
    timer = time()

    env = gym.make(env_name)

    agent = Agent((84, 84, 4), env.action_space.n, env_name, load_weights=True, sarsa=True)
    count = 0
    _ = 0
    av_score = 0

    agent.load_state(True, True)
    for episode in tqdm(range(episodes), leave=False, mininterval=0.001):
        obsv = process_state(env.reset())
        current_state = np.array([obsv, obsv, obsv, obsv])
        score = 0
        randActions = 0
        done = False
        steps = _
        loss = []
        epsilon = 0
        while not done:
            _ += 1
            # env.render()  # if episodes - episode - 1 <= 3 else None

            action, randPercentage = agent.action(np.asarray([current_state]), _)
            if randPercentage:
                randActions = randActions + 1

            obsv, reward, done, info = env.step(action)
            obsv = process_state(obsv)
            next_state = get_next_state(current_state, obsv)
            next_action, nothing = agent.action(np.asarray([next_state]), _)

            epsilon = agent.greedyEpsilon()
            if _ % 4 == 0:
                count += 1
                loss.append(agent.trainSarsa(np.asarray([current_state]), action, reward, np.asarray([next_state]),
                                             next_action, done))
                # DQN
                # loss.append(agent.trainDQN(np.asarray([current_state]), action, reward, np.asarray([next_state]),
                #                            done))
                # print("Steps : ", _, "\tCount = ", count)
                if count == TARGET_UPDATE_PERIOD:
                    count = 0
                    agent.save()

            current_state = next_state
            score += reward
        steps = _ - steps

        agent.save()
        # env.close()
        timer = time() - timer
        av_score = (av_score + score) / 2 if episode != 0 else score
        print("\t", episode + 1, "\tTotalReward = ", score, "\tSteps: ", steps, "\tLoss: {:.4f}".format(np.mean(loss)),
              "\tMoving Avg: {:.2f}".format(av_score), "\tRandom actions: {:.0f}".format(100 * randActions/steps), "%",
              "\tLast epsilon: {:.4f}".format(epsilon),
              "\tTime: %d" % (timer / 60), "\b:{:.0f}".format((timer % 60)))
        log.write(str(episode + 1) + "\tTotalReward = " + str(score) + "\tSteps: " + str(
            steps) + "\tLoss: {:.4f}".format(np.mean(loss)) + "\tMoving Avg: {:.2f}".format(av_score) +
                  "\tLast epsilon: {:.4f}".format(epsilon) +
                  "\tTime: %d" % int(timer / 60) + ":{:.0f} \n".format((timer % 60)))
        timer = time()
        del loss

        if episode % 500 == 0 and episode != 0 and episode != episodes - 1:
            print("Continuous save")
            agent.save_state(ongoing=True, sarsa=True)
    env.close()
    print("\n\nTotal Steps = ", _)
    log.close()

    del timer
    del current_state
    del next_state
    del count
    del _
    del steps
    del log
    del env
    gc.collect()

    agent.save_state(ongoing=False, sarsa=True)


def test(game, dqn, ddqn, dueling, both, sarsa):
    algo = ""
    if dqn:
        algo = "DQN"
    elif ddqn:
        algo = "DDQN"
    elif dueling:
        algo = "Dueling"
    elif both:
        ddqn = True
        dueling = True
        algo = "DDQN+Dueling"
    elif sarsa:
        algo = "Deep Sarsa"

    print("Testing " + algo)

    env_name = game + "-v0"
    env = gym.make(env_name)

    agent = Agent((84, 84, 4), env.action_space.n, env_name, epsilon=0, load_weights=True, test=True,
                  ddqn=ddqn, dueling=dueling, sarsa=sarsa)

    run = 'y'
    i = 0
    while run == 'y' or run == 'Y':
        try:
            obsv = process_state(env.reset())
            current_state = np.array([obsv, obsv, obsv, obsv])
            done = False
            score = _ = reward = 0
            while not done:
                _ += 1
                env.render()
                # sleep(0.4)
                action, nothing = agent.action(np.asarray([current_state]), _)
                # if action == 0:
                #     print("NOOP")
                # elif action == 1:
                #     print("FIRE")
                # elif action == 2:
                #     print("RIGHT")
                # elif action == 3:
                #     print("LEFT")
                # elif action == 4:
                #     print("RIGHTFIRE")
                # elif action == 5:
                #     print("LEFTFIRE")
                # print(action)

                # open("Logs/example.txt", 'a')
                # with open('Logs/example.txt', 'r') as readFile:
                #     data = readFile.read().split("\n")
                #     data = list(filter(None, data))
                #
                #     # print(data)
                #     if len(data) >= 20:
                #         with open('Logs/example.txt', 'w') as writeFile:
                #             data.pop(0)
                #             data.append(str(action))
                #
                #             for i in range(len(data)):
                #                 text = data[i].split(",")[0] + "," + str(i + 1) + "\n"
                #                 # print("text ", text)
                #                 writeFile.write(text)
                #
                #             # for i in range(len(data)):
                #             #     text = data[i] + ", " + str(i) + "\n"
                #             #     # print(i, data[i])
                #             #     writeFile.write(text)
                #             # writeFile.close()
                #     else:
                #         with open('Logs/example.txt', 'w') as writeFile:
                #             print(action)
                #             data.append(str(action))
                #             print(data)
                #             for i in range(len(data)):
                #                 text = data[i].split(",")[0] + "," + str(i + 1) + "\n"
                #                 print("text ", text)
                #                 writeFile.write(text)
                #             # writeFile.close()
                #     readFile.close()

                obsv, reward, done, info = env.step(action)
                obsv = process_state(obsv)
                next_state = get_next_state(current_state, obsv)

                current_state = next_state
                score += reward

            print("Total Reward: ", score, "\nSteps: ", _)
            # env.close()
        except KeyboardInterrupt:
            # env.step(env.action_space.sample())
            print()
        i = i + 1
        if i <= 3:
            run = "y"
        else:
            run = input("\nAgain? (Y/N) : ")
            i = 0

    # print("Exiting Environment.")
    env.close()


def process_state(observation):
    img = np.asanyarray(Image.fromarray(observation, 'RGB').convert('L').resize((84, 84)))
    # img = np.delete(img, np.s_[-13:], 0)
    # img = np.delete(img, np.s_[:13], 0)
    # img = np.delete(img, np.s_[-10:], 0)
    # img = np.delete(img, np.s_[:16], 0)
    # print(img.shape)
    # Image._show(Image.fromarray(img))

    return img

    # Image._show(Image.fromarray(img))
    # Image._show((Image.open(observation).convert('L').resize((84, 110))))
    # Image._showxv(Image.fromarray(observation, 'RGB').convert('L').resize((84, 110)), "BEFORE CROPPING")


def get_next_state(current, observation):
    return np.append(current[1:], [observation], axis=0)


# def processed_screen():
#     name = "SpaceInvaders-v0"
#     env = gym.make(name)
#     env.reset()
#     for i in range(400):
#         env.step(env.action_space.sample())
#     screen = env.render("rgb_array")
#     Image.fromarray(screen).save(name + ".png")
#     Image.fromarray(process_state(screen)).show()


if __name__ == '__main__':
    # processed_screen()
    try:
        game_name = sys.argv[1]
    except IndexError:
        game_name = "SpaceInvaders"
    if input("1. Train Agent\n2. Run Test\n\n: ") == '1':
        alg = input("1. DQN\n2. DDQN\n3. Dueling\n4. Both\n5. Deep SARSA\n\n: ")
        if alg == '1':
            main(game_name, True, False, False, False)
        elif alg == "2":
            main(game_name, False, True, False, False)
        elif alg == "3":
            main(game_name, False, False, True, False)
        elif alg == "4":
            main(game_name, False, False, False, True)
        elif alg == "5":
            sarsa(game_name)

    else:
        alg = input("1. DQN\n2. DDQN\n3. Dueling\n4. Both\n5. Deep SARSA\n\n: ")
        if alg == '1':
            test(game_name, True, False, False, False, False)
        elif alg == "2":
            test(game_name, False, True, False, False, False)
        elif alg == "3":
            test(game_name, False, False, True, False, False)
        elif alg == "4":
            test(game_name, False, False, False, True, False)
        elif alg == "5":
            test(game_name, False, False, False, False, True)
        # test(game_name)
        # process_state("SC0.png")
        # processed_screen()
