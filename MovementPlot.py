import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

style.use('seaborn-whitegrid')
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)

# with open('Logs/example.txt', 'w') as writeFile:
#     writeFile.write("string" + " + " + "cislo\n")
#     writeFile.write("string" + " + " + "cislo\n")
#     writeFile.write("string" + " + " + "cislo\n")
#     writeFile.write("string" + " + " + "cislo\n")
#     writeFile.write("string" + " + " + "cislo\n")
# with open('Logs/example.txt', 'r') as readFile:
#     # data = readFile.read().split("\n")
#     data = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
#     # print(data)
#     for i in range(len(data)):
#         # print(data[i], i)
#         with open('Logs/example.txt', 'a') as writeFile:
#             writeFile.write(data[i] + ", " + str(i) + "\n")


def animate(i):
    graph_data = open('Logs/example.txt', 'r').read()
    lines = graph_data.split('\n')
    xs = []
    ys = []
    # for line in lines:
    #     if len(line) > 1:
    #         x = line.split(',')[0]
    #         # x = line.strip()
    #         xs.append(x)
    #         ys.append(len(line))
    #     ax1.clear()
    #     ax1.plot(xs, ys)

    for line in lines:
        if len(line) > 1:
            x, y = line.split(',')
            # x = line.strip()
            # y = len(line)
            xs.append(int(y))
            ys.append(int(x))
        ax1.clear()
        plt.xlim(-1, 6)
        plt.ylim(0, 21)
        plt.text(-0.2, -2, "NOOP")
        plt.text(0.7, -2, "FIRE")
        plt.text(1.7, -2, "RIGHT")
        plt.text(2.7, -2, "LEFT")
        plt.text(3.4, -2, "RIGHTFIRE")
        plt.text(4.7, -2, "LEFTFIRE")
        plt.plot(ys, xs, 'ro', color="black", alpha=0.4)

        # plt.scatter(ys, xs, color="black", alpha=0.4)


ani = animation.FuncAnimation(fig, animate, interval=1)
plt.show()
